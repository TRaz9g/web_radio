package org.raduni.webradio;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

/**
 * Created by TRaz on 03.05.17.
 * Have to work
 */

public class PlayerActivity extends AppCompatActivity {
    private WebView webView;
    private ImageView stationCover;
    private ImageView adImage;
    private boolean isPaused = false;

    private MediaPlayer mediaPlayer = new MediaPlayer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        volumeLevel= mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        updateStation();
    }

    private int volumeLevel = 0;
    private void updateStation() {
        stationCover = (ImageView) findViewById(R.id.player_coverImg);
        adImage = (ImageView) findViewById(R.id.player_adView);
        webView = (WebView) findViewById(R.id.player_webView);

        ((TextView) findViewById(R.id.player_stationName)).setText(App.currentRadioStation.getName());
        ((TextView) findViewById(R.id.player_universityName)).setText(App.currentRadioStation.getUniversity());

        findViewById(R.id.player_playImg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPaused) {
                    isPaused = false;

                    ((ImageView) view).setImageResource(R.drawable.ic_pause);
//                    mediaPlayer.start();

                    AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volumeLevel, 0);
                } else {
                    isPaused = true;

                    ((ImageView) view).setImageResource(R.drawable.ic_play);
//                    mediaPlayer.pause();

                    AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                    volumeLevel= mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
                }
            }
        });

        findViewById(R.id.player_backImg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        findViewById(R.id.player_previousImg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = App.radioStations.indexOf(App.currentRadioStation);
                if (position - 1 >= 0) {
                    App.currentRadioStation = App.radioStations.get(position - 1);
                } else {
                    App.currentRadioStation = App.radioStations.get(App.radioStations.size() - 1);
                }

                findViewById(R.id.player_loading).setVisibility(View.VISIBLE);
                findViewById(R.id.player_controls).setVisibility(View.INVISIBLE);

                AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volumeLevel, 0);

                updateStation();
            }
        });

        findViewById(R.id.player_nextImg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = App.radioStations.indexOf(App.currentRadioStation);
                if (position + 1 < App.radioStations.size()) {
                    App.currentRadioStation = App.radioStations.get(position + 1);
                } else {
                    App.currentRadioStation = App.radioStations.get(0);
                }

                findViewById(R.id.player_loading).setVisibility(View.VISIBLE);
                findViewById(R.id.player_controls).setVisibility(View.INVISIBLE);

                AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volumeLevel, 0);

                updateStation();
            }
        });

        playSong(App.currentRadioStation);

        loadStationCover();
        updateSocialIcons();
    }

    private void updateSocialIcons() {
        final String webLink = App.currentRadioStation.getSocialWebLink();
        if (webLink != null && !webLink.isEmpty()) {
            findViewById(R.id.player_social_web).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(webLink));
                    startActivity(intent);
                }
            });
        } else {
            findViewById(R.id.player_social_web).setVisibility(View.GONE);
        }

        final String facebookLink = App.currentRadioStation.getSocialFacebookLink();
        if (facebookLink != null && !facebookLink.isEmpty()) {
            findViewById(R.id.player_social_facebook).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(facebookLink));
                    startActivity(intent);
                }
            });
        } else {
            findViewById(R.id.player_social_facebook).setVisibility(View.GONE);
        }

        final String twitterLink = App.currentRadioStation.getSocialTwitterLink();
        if (twitterLink != null && !twitterLink.isEmpty()) {
            findViewById(R.id.player_social_twitter).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(twitterLink));
                    startActivity(intent);
                }
            });
        } else {
            findViewById(R.id.player_social_twitter).setVisibility(View.GONE);
        }

        final String instagramLink = App.currentRadioStation.getSocialInstagramLink();
        if (instagramLink != null && !instagramLink.isEmpty()) {
            findViewById(R.id.player_social_instagram).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(instagramLink));
                    startActivity(intent);
                }
            });
        } else {
            findViewById(R.id.player_social_instagram).setVisibility(View.GONE);
        }
    }

    private void loadStationCover() {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new RoundedBitmapDisplayer(20))
                .build();

        ImageLoader.getInstance().displayImage(App.currentRadioStation.getCoverLink(), stationCover, options);
        ImageLoader.getInstance().displayImage(App.currentRadioStation.getAd(), adImage, options);
    }

    private void playSong(RadioStation radioStation) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        webView.loadUrl(radioStation.getStreamLink());
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                findViewById(R.id.player_loading).setVisibility(View.GONE);
                findViewById(R.id.player_controls).setVisibility(View.VISIBLE);
            }
        });

//        try {
//            mediaPlayer.reset();
//            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//            mediaPlayer.setDataSource(radioStation.getStreamLink());
//            mediaPlayer.prepareAsync();
//            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                @Override
//                public void onPrepared(MediaPlayer player) {
//                    findViewById(R.id.player_loading).setVisibility(View.GONE);
//                    findViewById(R.id.player_controls).setVisibility(View.VISIBLE);
//
//                    player.start();
//                }
//            });
//        } catch (IOException e) {
//            Log.e(getString(R.string.app_name), e.getMessage());
//        } catch (Exception e) {
//            e.printStackTrace();
//
//            Log.e(getString(R.string.app_name), e.getMessage());
//
//            new AlertDialog.Builder(this, R.style.PlayerDialogStyle)
//                    .setMessage("Can't play this radio station")
//                    .setCancelable(false)
//                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            dialog.cancel();
//                            onBackPressed();
//                        }
//                    })
//                    .show();
//        }
    }

    @Override
    public void onBackPressed() {
        mediaPlayer.stop();
        mediaPlayer.release();

        if (webView != null) {
            webView.destroy();
            webView = null;
        }

        super.onBackPressed();
    }
}