package org.raduni.webradio;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.List;

/**
 * Created by TRaz on 10.05.17.
 * Have to work
 */

class RadioStationAdapter extends BaseAdapter {
    private Context context;
    private List<RadioStation> radioStations;

    private DisplayImageOptions options;

    RadioStationAdapter(Context context, List<RadioStation> radioStations) {
        this.context = context;
        this.radioStations = radioStations;

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new RoundedBitmapDisplayer(20))
                .build();
    }

    @Override
    public int getCount() {
        return radioStations.size();
    }

    @Override
    public RadioStation getItem(int i) {
        return radioStations.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.line_radio_station, viewGroup, false);
        }

        RadioStation radioStation = getItem(i);

        ((TextView) view.findViewById(R.id.line_radioStation_nameLabel)).setText(radioStation.getName());
        ((TextView) view.findViewById(R.id.line_radioStation_universityLabel)).setText(radioStation.getUniversity());

        loadStationCover(radioStation, (ImageView) view.findViewById(R.id.line_radioStation_coverImage));

        return view;
    }

    void update(List<RadioStation> radioStations) {
        this.radioStations = radioStations;
        notifyDataSetChanged();
    }

    private void loadStationCover(RadioStation station, ImageView view) {
        ImageLoader.getInstance().displayImage(station.getCoverLink(), view, options);
    }
}