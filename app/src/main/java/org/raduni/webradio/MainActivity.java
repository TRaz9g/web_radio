package org.raduni.webradio;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by TRaz on 28.04.17.
 * Have to work
 */
public class MainActivity extends AppCompatActivity {
    private View loadingLayer;

    private ListView stationListView;
    private RadioStationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadingLayer = findViewById(R.id.main_loading);

        adapter = new RadioStationAdapter(MainActivity.this, App.radioStations);
        stationListView = (ListView) findViewById(R.id.main_stationList);
        stationListView.setAdapter(adapter);

        stationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                App.currentRadioStation = adapter.getItem(i);

                startActivity(new Intent(MainActivity.this, PlayerActivity.class));
            }
        });

        requestRadioStationsList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_main_updateItem) {
            requestRadioStationsList();
        }

        return super.onOptionsItemSelected(item);
    }

    private void requestRadioStationsList() {
        loadingLayer.setVisibility(View.VISIBLE);
        stationListView.setVisibility(View.GONE);

        new UpdateStationsTask().execute(App.STATIONS_JSON_URL);
    }

    class UpdateStationsTask extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            JSONObject payload = new JsonParser().getJSONFromUrl(urls[0]);

            if (payload != null) {
                try {
                    JSONArray stationJsonArray = payload.getJSONArray("stations");
                    List<RadioStation> radioStations = new ArrayList<>();

                    for (int i = 0; i < stationJsonArray.length(); i++) {
                        JSONObject stationJsonObject = stationJsonArray.getJSONObject(i);

                        RadioStation radioStation = new RadioStation();
                        radioStation.setId(stationJsonObject.getInt("id"));
                        radioStation.setName(stationJsonObject.getString("nome"));
                        radioStation.setAd(stationJsonObject.getString("ad"));
                        radioStation.setStreamLink(stationJsonObject.getString("stream"));
                        radioStation.setStreamType(stationJsonObject.getString("tipo"));
                        radioStation.setUniversity(stationJsonObject.getString("universita"));
                        radioStation.setSocialWebLink(stationJsonObject.getString("web"));
                        radioStation.setCoverLink(stationJsonObject.getString("logo"));
                        radioStation.setSocialFacebookLink(stationJsonObject.getString("fb"));
                        radioStation.setSocialInstagramLink(stationJsonObject.getString("instagram"));
                        radioStation.setSocialTwitterLink(stationJsonObject.getString("twitter"));

                        radioStations.add(radioStation);
                    }

                    Collections.shuffle(radioStations, new Random(System.nanoTime()));

                    App.radioStations = radioStations;

                    return "ok";
                } catch (JSONException e) {
                    e.printStackTrace();

                    return "wrong json format";
                }
            } else {
                return "connection error";
            }
        }

        protected void onPostExecute(String result) {
            switch (result) {
                case "ok":
                    loadingLayer.setVisibility(View.GONE);
                    stationListView.setVisibility(View.VISIBLE);

                    updateListView();
                    break;
                case "wrong json format":
                    loadingLayer.setVisibility(View.GONE);
                    stationListView.setVisibility(View.GONE);

                    new AlertDialog.Builder(MainActivity.this, R.style.PlayerDialogStyle)
                            .setMessage("Invalid data format")
                            .setCancelable(false)
                            .setNegativeButton("Retry", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    requestRadioStationsList();
                                }
                            })
                            .show();
                    break;
                case "connection error":
                    loadingLayer.setVisibility(View.GONE);
                    stationListView.setVisibility(View.GONE);

                    new AlertDialog.Builder(MainActivity.this, R.style.PlayerDialogStyle)
                            .setMessage("Can't update radio stations")
                            .setCancelable(false)
                            .setNegativeButton("Retry", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    requestRadioStationsList();
                                }
                            })
                            .show();
                    break;
            }
        }
    }

    private void updateListView() {
        adapter.update(App.radioStations);
    }
}