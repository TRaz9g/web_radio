package org.raduni.webradio;

/**
 * Created by TRaz on 28.04.17.
 * Have to work
 */

class RadioStation {
    private int id;

    private String name;
    private String university;
    private String ad;

    private String streamLink;
    private String streamType;
    private String coverLink;

    private String socialWebLink;
    private String socialFacebookLink;
    private String socialTwitterLink;
    private String socialInstagramLink;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getStreamLink() {
        return streamLink;
    }

    public void setStreamLink(String streamLink) {
        this.streamLink = streamLink;
    }

    public String getStreamType() {
        return streamType;
    }

    public void setStreamType(String streamType) {
        this.streamType = streamType;
    }

    public String getCoverLink() {
        return coverLink;
    }

    public void setCoverLink(String coverLink) {
        this.coverLink = coverLink;
    }

    public String getSocialWebLink() {
        return socialWebLink;
    }

    public void setSocialWebLink(String socialWebLink) {
        this.socialWebLink = socialWebLink;
    }

    public String getSocialFacebookLink() {
        return socialFacebookLink;
    }

    public void setSocialFacebookLink(String socialFacebookLink) {
        this.socialFacebookLink = socialFacebookLink;
    }

    public String getSocialTwitterLink() {
        return socialTwitterLink;
    }

    public void setSocialTwitterLink(String socialTwitterLink) {
        this.socialTwitterLink = socialTwitterLink;
    }

    public String getSocialInstagramLink() {
        return socialInstagramLink;
    }

    public void setSocialInstagramLink(String socialInstagramLink) {
        this.socialInstagramLink = socialInstagramLink;
    }
}