package org.raduni.webradio;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class SplashActivity extends AppCompatActivity {
    private ImageView logoImage, secondLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        logoImage = (ImageView) findViewById(R.id.splash_logoImg);
        secondLogo = (ImageView) findViewById(R.id.splash_secondLogoImg);
        animateLogos();

        openMainScreen();
    }

    private void openMainScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }, 2000);
    }

    private void animateLogos() {
        logoImage.animate()
                .alphaBy(0)
                .alpha(1)
                .setDuration(500)
                .setStartDelay(300)
                .start();

        secondLogo.animate()
                .alphaBy(0)
                .alpha(1)
                .setDuration(500)
                .setStartDelay(1000)
                .start();
    }
}